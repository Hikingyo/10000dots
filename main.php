<?php
namespace Square;

const FICHIER_COORD = "data/exercice6.txt";
$dateDebut = microtime(true);
$coord = array();
$xMax = 0;
$xMin = 0;
$yMax = 0;
$yMin = 0;
$nbCarre = 0;
$carre = array();
echo "Parmis 10 000 points repartis aleatoirement, combien y a-t-il de carre unique ?\n";
echo "Temps de reponse estime a moins d'une minute, veuillez patienter.\n";

//Ouverture du fichier
echo "Traitement du fichier.\n";
$file = fopen(FICHIER_COORD, 'r');

// Si le fichier existe
if ($file) {
    while (!feof($file)) {

        // Lecture de la ligne
        $line = trim(fgets($file));
        if (!empty($line)) {
            $c = explode(" ", $line);
            // Détermination de la plus grande dimension possible pour les carrés
            $x = (int)$c[0];
            if ($x > $xMax) {
                $xMax = $x;
            } elseif ($x < $xMin) {
                $xMin = $x;
            }


            $y = (int)$c[1];
            if ($y > $yMax) {
                $yMax = $y;
            } elseif ($y < $yMin) {
                $yMin = $y;
            }

            $largeurChamps = abs($xMax) + abs($xMin);
            $hauteurChamps = abs($yMax) + abs($yMin);
            $largeurChamps > $hauteurChamps ? $longueurMaxPow2 = $largeurChamps * $largeurChamps : $longueurMaxPow2 = $hauteurChamps * $hauteurChamps;
            // Stockage des points
            if (!isset($coord[$x][$y])) {
                $coord[$x][$y] = TRUE;
            }

        }
    }
}
// Fermeture du fichier
fclose($file);

// Tri des coordonnées
ksort($coord);
array_walk($coord, 'ksort');

//Recherche des carrés
echo "Recherche des carres uniques.\n";
foreach ($coord as $x => $ay) {
    foreach ($ay as $y => $p) {
        // Recherche d'un premier côté
        foreach ($coord as $x2 => $ay2) {
            foreach ($ay2 as $y2 => $p2) {
                // Si le segment est trop grand ou nul
                $longueurABPow2 = (($x - $x2) * ($x - $x2)) + (($y - $y2) * ($y - $y2));
                if ($longueurABPow2 <= $longueurMaxPow2 && $longueurABPow2 != 0) {
                    // Recherche d'un deuxiéme côté
                    $x3 = $x2 - ($y2 - $y);
                    $y3 = $y2 + ($x2 - $x);
                    // Si le troisième sommet existe
                    if (isset($coord[$x3][$y3])) {
                        // Recherche du 4eme sommet correspondant
                        $x4 = $x - ($y2 - $y);
                        $y4 = $y + ($x2 - $x);
                        // Si le quatrième sommet existe, on a un carré.
                        if (isset($coord[$x4][$y4])) {
                            $nbCarre++;
                            if($nbCarre%10 == 0 && $nbCarre != 0){
                                echo $nbCarre." carres ont ete trouves.\n";
                            }
                            $carre[] = array(
                                array($x, $y),
                                array($x2, $y2),
                                array($x3, $y3),
                                array($x4, $y4)
                            );
                        }
                    }
                    // Recherche autre d'un deuxiéme côté
                    $x3 = $x2 + ($y2 - $y);
                    $y3 = $y2 - ($x2 - $x);
                    // Si le troisième sommet existe
                    if (isset($coord[$x3][$y3])) {
                        // Recherche du 4eme sommet correspondant
                        $x4 = $x + ($y2 - $y);
                        $y4 = $y - ($x2 - $x);
                        // Si le quatrième sommet existe, on a un carré.
                        if (isset($coord[$x4][$y4])) {
                            $nbCarre++;
                            if($nbCarre%10 == 0 && $nbCarre != 0){
                                echo $nbCarre." carres ont ete trouves.\n";
                            }
                            $carre[] = array(
                                array($x, $y),
                                array($x2, $y2),
                                array($x3, $y3),
                                array($x4, $y4)
                            );
                        }
                    }
                }

            }
        }
        unset($coord[$x][$y]);
    }
}
// On supprime les permutation ABCD / DABC
$nbCarre /= 2;
// Calcule du temps d'éxecution
$duree = microtime(true) - $dateDebut;

// Affichage
echo "Temps d'execution : " . $duree . "\n";
echo "Nombre de carres trouves : " . $nbCarre . "\n";
//var_dump($carre);